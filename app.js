const express = require('express');
const cors = require('cors');
const { Pool } = require('pg');
 
const {
  EventStoreDBClient,
  jsonEvent,
  FORWARDS,
  START,
} = require("@eventstore/db-client");
require('dotenv').config()

const url = process.env.EVENT_STORE_URL
const port = process.env.EVENT_STORE_PORT
const tls = process.env.EVENT_STORE_TLS

const client = EventStoreDBClient.connectionString(
  `esdb://${url}:${port}?tls=${tls}`
);

const pool = new Pool({
  user: process.env.DATABASE_USER,
  host: process.env.DATABASE_HOST,
  database: process.env.DATABASE_NAME,
  password: process.env.DATABASE_PASSWORD,
  port: process.env.DATABASE_PORT || 5432,
});

const app = express();
const PORT = process.env.PORT || 3000;

app.use(cors());
app.use(express.json());

function storeEvent(eventData) {
  const ev = jsonEvent({
    type: "payment",
    data: eventData,
  });
  
  const STREAM_NAME = "es_payment_streams";
  client.appendToStream(STREAM_NAME, ev)
}

const getColumnNames = async (tableName) => {
  const res = await pool.query(`
    SELECT column_name
    FROM information_schema.columns
    WHERE table_name = $1
  `, [tableName]);

  return res.rows.map(row => row.column_name);
};

async function updateViews(eventData) {
  const result = await pool.query(`
    SELECT table_name 
    FROM information_schema.tables 
    WHERE table_schema = 'public' AND table_type = 'BASE TABLE';
  `);

  const tableNames = result.rows.map(row => row.table_name);
  

  for(const table of tableNames) {
    let sb = "INSERT INTO " + table;
    let part1 = " (";
    let part2 = "VALUES (";

    let vals = [];

    if(table !== "user_tbl") {
      const columnNames = await getColumnNames(table);
      let i = 1;

      for (const columnName of columnNames) {
        part1 += columnName + ", "; 
        part2 += "$" + i + ", ";
        i++;

        vals.push(eventData[columnName]);
      }  
    }
    
    let query = sb + replaceLastCommaWithParenthesis(part1) + replaceLastCommaWithParenthesis(part2);
    
    try {
      await pool.query(query, vals);
      console.log('Values inserted successfully');
    } catch (error) {
      console.error('Error inserting values:', error);
   }
  }
}

const replaceLastCommaWithParenthesis = (inputString) => {
  // Find the last index of the comma
  const lastCommaIndex = inputString.lastIndexOf(',');

  // If no comma is found, return the input string as is
  if (lastCommaIndex === -1) {
    return inputString;
  }

  // Replace the last comma with a closing parenthesis
  return inputString.slice(0, lastCommaIndex) + ')' + inputString.slice(lastCommaIndex + 1);
};


function updateViewDB(eventData) {
  const { action, username } = eventData;

  if(action == "add" ) {
    // Check if user already exists in the table
    const userExistsQuery = 'SELECT * FROM user_tbl WHERE username = $1';
    const userExistsResult = pool.query(userExistsQuery, [username]);
    console.log(userExistsResult);
    
    userExistsResult.then(res => {
      if (res.rows.length === 0) {
        // If user does not exist, create entry and set isActive to true
        const insertQuery = 'INSERT INTO user_tbl (username, isActive) VALUES ($1, $2)';
        pool.query(insertQuery, [username, true]);
      } else {
        // If user exists, but isActive is set to false, set it to true
        const userIsActive = res.rows[0].isactive;

        if (!userIsActive) {
          const updateQuery = 'UPDATE user_tbl SET isActive = true WHERE username = $1';
          pool.query(updateQuery, [username]);
        }  
      }
    })

  } else if(action == "delete") {
    // Check if user already exists in the table
    const userExistsQuery = 'SELECT * FROM user_tbl WHERE username = $1';
    const userExistsResult = pool.query(userExistsQuery, [username]);

    userExistsResult.then(res => {
      if (res.rows.length > 0) {
        // If user exists and isActive is true, set it to false
        const userIsActive = res.rows[0].isactive;
      
        if (userIsActive) {
          const updateQuery = 'UPDATE user_tbl SET isActive = false WHERE username = $1';
          pool.query(updateQuery, [username]);
        }
      }  
    })
  }
}

pool.query("CREATE TABLE IF NOT EXISTS user_tbl(username VARCHAR(255), isActive BOOLEAN)", (err, res) => {
    console.log(err, res);
});


app.get('/postback', (req, res) => {
  const eventData = req.query;
  console.log(eventData);
  storeEvent(eventData);
  updateViewDB(eventData);
  updateViews(eventData);

  res.send("ok");
});

app.get("/getStreamEvents", async (req, res) => {
  try {
    const events = await readAllEvents();
    res.json(events);
  } catch (error) {
    res.status(500).json({ error: "Failed to read events" });
  }
});

async function readAllEvents() {
  const events = [];

  try {
    // Read events from the stream
    const eventStream = client.readStream("es_payment_streams", {
      direction: "forwards",
      fromRevision: "start",
      maxCount: 100 // Number of events to read, adjust as needed
    });

    // Loop through the events and process them
    for await (const resolvedEvent of eventStream) {
      const event = resolvedEvent.event;
      if (event) {
        events.push({
          id: event.id,
          type: event.type,
          data: event.data,
        });
      }
    }
  } catch (error) {
    console.error(`Error reading events: ${error}`);
  }
  return events;
}

app.get("/tableNames", async (req, res) => {
  const result = await pool.query(`
    SELECT table_name 
    FROM information_schema.tables 
    WHERE table_schema = 'public' AND table_type = 'BASE TABLE';
  `);

  const tableNames = result.rows.map(row => row.table_name);
  res.json({ tables: tableNames });
})

app.get("/getView/:tableName", async (req, res) => {
  const { tableName } = req.params;

  if (!tableName) {
    return res.status(400).json({ error: 'Table name is required' });
  }

  const getTableQuery = `SELECT * FROM ${tableName}`;
  const getColumnNamesQuery = `
    SELECT column_name
    FROM information_schema.columns
    WHERE table_name = $1
  `;

  try {
    const tableData = await pool.query(getTableQuery);
    const columnNamesResult = await pool.query(getColumnNamesQuery, [tableName]);

    const columnNames = columnNamesResult.rows.map(row => row.column_name);
    const tableValues = tableData.rows;

    // Create the result object with column names as keys and empty arrays as values
    const result = columnNames.reduce((acc, columnName) => {
      acc[columnName] = [];
      return acc;
    }, {});

    // Populate the arrays with values from the table
    tableValues.forEach(row => {
      columnNames.forEach(columnName => {
        result[columnName].push(row[columnName]);
      });
    });

    res.status(200).json(result);
  } catch (err) {
    console.error('Error retrieving table data:', err);
    res.status(500).json({ error: 'Error retrieving table data' });
  }
});

app.post("/deleteViewDB", async (req, res) => {
  const { tableName } = req.body;

  if (!tableName) {
    return res.status(400).json({ error: 'Table name is required' });
  }

  try {
    const query = `DROP TABLE IF EXISTS ${tableName}`;
    await pool.query(query);

    res.status(200).json({ message: `Table ${tableName} deleted successfully` });
  } catch (error) {
    console.error('Error deleting table:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

app.post("/dropViewDbData", async (req, res) => {
  const { tableName } = req.body;

  if (!tableName) {
    return res.status(400).json({ error: 'Table name is required' });
  }

  try {
    const query = `DELETE FROM ${tableName}`;
    await pool.query(query);

    res.status(200).json({ message: `Table ${tableName} deleted successfully` });
  } catch (error) {
    console.error('Error deleting table:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});


app.post("/createViewDB", async (req, res) => {
  const { tableName, columns, includeHistoricalEvents } = req.body;

  if (!tableName || !Array.isArray(columns) || columns.length === 0) {
    return res.status(400).json({ error: "Invalid input" });
  }

  const columnsDefinition = columns
    .map((column) => `${column.name} ${column.type}`)
    .join(", ");
  const createTableQuery = `CREATE TABLE ${tableName} (${columnsDefinition});`;

  try {
    await pool.query(createTableQuery);
    //res.status(201).json({ message: `Table ${tableName} created successfully` });
  } catch (error) {
    console.error("Error creating table:", error);
    res.status(500).json({ error: "Failed to create table" });
  }

  console.log(includeHistoricalEvents)
  if(includeHistoricalEvents) {
    console.log("called");
    const events = await readAllEvents();
    
    for(let ev of events) {
      let sb = "INSERT INTO " + tableName;
      let part1 = " (";
      let part2 = "VALUES (";

      let vals = [];
      
      //console.log("events");
      //console.log(ev);

      if(tableName !== "user_tbl") {  
        const columnNames = await getColumnNames(tableName);
        let i = 1;

        for (const columnName of columnNames) {
          part1 += columnName + ", "; 
          part2 += "$" + i + ", ";
          i++;

          vals.push(ev.data[columnName]);
        }  
      }
    
      let query = sb + replaceLastCommaWithParenthesis(part1) + replaceLastCommaWithParenthesis(part2);
    
      try {
        await pool.query(query, vals);
        console.log('Values inserted successfully');
      } catch (error) {
        console.error('Error inserting values:', error);
      }
    }
  }
});


app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});

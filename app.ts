import express from 'express';
import { Pool, QueryResult } from 'pg';
import { EventStoreDBClient, jsonEvent } from '@eventstore/db-client';
import dotenv from 'dotenv';

dotenv.config();

const url = process.env.EVENT_STORE_URL;
const port = process.env.EVENT_STORE_PORT;
const tls = process.env.EVENT_STORE_TLS;

const client = EventStoreDBClient.connectionString(`esdb://${url}:${port}?tls=${tls}`);

const pool = new Pool({
  user: process.env.DATABASE_USER,
  host: process.env.DATABASE_HOST,
  database: process.env.DATABASE_NAME,
  password: process.env.DATABASE_PASSWORD,
  port: 5432,
});

const app = express();
const PORT = process.env.PORT || 3000;
app.use(express.json());

/*
interface EventData {
  action: string;
  username: string;
}
*/

async function storeEvent(eventData: any) {
  const ev = jsonEvent({ type: "payment", data: eventData });
  await client.appendToStream("es_payment_streams", ev);
}

async function updateViewDB(eventData: any) {
  console.log(eventData);
  // Your implementation here
  const { action, username } = eventData;

  if(action == "add" ) {
    // Check if user already exists in the table
    const userExistsQuery = 'SELECT * FROM user_tbl WHERE username = $1';
    const userExistsResult = await pool.query(userExistsQuery, [username]);

    if (userExistsResult.rows.length === 0) {
      // If user does not exist, create entry and set isActive to true
      const insertQuery = 'INSERT INTO user_tbl (username, isActive) VALUES ($1, $2)';
      await pool.query(insertQuery, [username, true]);
    } else {
      // If user exists, but isActive is set to false, set it to true
      const userIsActive = userExistsResult.rows[0].isactive;
      
      if (!userIsActive) {
        const updateQuery = 'UPDATE user_tbl SET isActive = true WHERE username = $1';
        await pool.query(updateQuery, [username]);
      }
    }
  } else if(action == "delete") {
    // Check if user already exists in the table
    const userExistsQuery = 'SELECT * FROM user_tbl WHERE username = $1';
    const userExistsResult = await pool.query(userExistsQuery, [username]);

    if (userExistsResult.rows.length > 0) {
      // If user exists and isActive is true, set it to false
      const userIsActive = userExistsResult.rows[0].isactive;
      
      if (userIsActive) {
        const updateQuery = 'UPDATE user_tbl SET isActive = false WHERE username = $1';
        await pool.query(updateQuery, [username]);
      }
    }
  }
}

async function handlePostbackRequest(req: express.Request, res: express.Response) {
  const eventData = req.query;
  await storeEvent(eventData);
  await updateViewDB(eventData);
  res.send("okay");
}

app.get('/postback', handlePostbackRequest);

pool.query('CREATE TABLE IF NOT EXISTS user_tbl(username VARCHAR(255), status VARCHAR(255))').then((result: QueryResult) => {
  console.log(result);
  pool.end();
}).catch((err: Error) => {
  console.error(err);
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
